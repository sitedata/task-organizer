{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TaskCalendarTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type
  TTaskCalendarTest = class(TTestCase)
    procedure TestDescriptionFolding;
    procedure TestCalculateBaseDateTime;
    procedure TestCalculateTaskTime;
    procedure TestFormatLocalTime;
  end;

implementation

uses TaskCalendar;

procedure TTaskCalendarTest.TestDescriptionFolding;
begin
  // simple tests
  AssertEquals('', DescriptionFolding(''));
  AssertEquals('a', DescriptionFolding('a'));
  AssertEquals('a b c', DescriptionFolding('a b c'));
  AssertEquals('a  b  c  ', DescriptionFolding('a  b  c  '));

  // escaping tests
  AssertEquals('a\\b\\nc', DescriptionFolding('a\b\nc'));
  AssertEquals('a\nb', DescriptionFolding('a'#13'b'));
  AssertEquals('a\nb', DescriptionFolding('a'#10'b'));
  AssertEquals('a\nb', DescriptionFolding('a'#13#10'b'));
  AssertEquals('a\n\nb', DescriptionFolding('a'#13#13'b'));
  AssertEquals('a\n\nb', DescriptionFolding('a'#13#10#13#10'b'));

  // line break tests
  // break after 75 characters if there's no space
  AssertEquals('123456789012345678901234567890123456789012345678901234567890123456789012345'#13#10#9'67890',
    DescriptionFolding('12345678901234567890123456789012345678901234567890123456789012345678901234567890'));
  // break after last space not after 75 characters line length
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 123456789 '#13#10#9'123456789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789')); // 70
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 1234567890 '#13#10#9'23456789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 1234567890 23456789')); // 71
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 12345678901 '#13#10#9'3456789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 12345678901 3456789')); // 72
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 123456789012 '#13#10#9'456789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 123456789012 456789')); // 73
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 1234567890123 '#13#10#9'56789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 1234567890123 56789')); // 74
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 12345678901234 '#13#10#9'6789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 12345678901234 6789')); // 75
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 '#13#10#9'123456789012345 789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 123456789012345 789')); // 76
  AssertEquals('a23456789 123456789 123456789 123456789 123456789 123456789 123456789 '#13#10#9'123456789 123456789 123456789 123456789 123456789 123456789 123456789 '#13#10#9'123456789 123456789',
    DescriptionFolding('a23456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789'));
  AssertEquals('123456789 123456789 123456789 123456789 123456789 123456789 123456789 '#13#10#9'123456789 123456789',
    DescriptionFolding('123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789'));
  AssertEquals('1234.\nmehrere\nZeilen\n\nmit Leerzeilen und ganz viel Text darin, sodass '#13#10#9'er länger als 75 Zeichen ist.',
    DescriptionFolding('1234.'#13#10'mehrere'#13#10'Zeilen'#13#10#13#10'mit Leerzeilen und ganz viel Text darin, sodass er länger als 75 Zeichen ist.'));
end;

procedure TTaskCalendarTest.TestCalculateBaseDateTime;
var BaseDate: TDateTime;
    Expected1: TDateTime;
begin
  BaseDate := EncodeDate(2020, 12, 31);
  Expected1 := BaseDate + EncodeTime(1, 0, 0, 0);
  AssertEquals(Expected1, CalculateBaseDateTime(BaseDate + EncodeTime(1, 0, 0, 0)));
  AssertEquals(Expected1, CalculateBaseDateTime(BaseDate + EncodeTime(1, 2, 3, 4)));
  AssertEquals(Expected1, CalculateBaseDateTime(BaseDate + EncodeTime(1, 59, 59, 999)));
end;

procedure TTaskCalendarTest.TestCalculateTaskTime;
var BaseDate: TDateTime;
    BaseDateTime: TDateTime;
begin
  BaseDate := EncodeDate(2020, 12, 31);
  BaseDateTime := BaseDate + EncodeTime(1, 2, 0, 0);
  AssertEquals(BaseDate + EncodeTime(1,  2, 0, 0), CalculateTaskTime(BaseDateTime, 10, True,  0));
  AssertEquals(BaseDate + EncodeTime(1, 12, 0, 0), CalculateTaskTime(BaseDateTime, 10, False, 0));
  AssertEquals(BaseDate + EncodeTime(1, 17, 0, 0), CalculateTaskTime(BaseDateTime, 15, False, 0));
  AssertEquals(BaseDate + EncodeTime(1, 12, 0, 0), CalculateTaskTime(BaseDateTime, 10, True,  1));
  AssertEquals(BaseDate + EncodeTime(1, 22, 0, 0), CalculateTaskTime(BaseDateTime, 10, False, 1));
  AssertEquals(BaseDate + EncodeTime(1, 22, 0, 0), CalculateTaskTime(BaseDateTime, 10, True,  2));
  AssertEquals(BaseDate + EncodeTime(1, 32, 0, 0), CalculateTaskTime(BaseDateTime, 10, False, 2));
  AssertEquals(BaseDate + EncodeTime(1, 32, 0, 0), CalculateTaskTime(BaseDateTime, 10, True,  3));
  AssertEquals(BaseDate + EncodeTime(1, 42, 0, 0), CalculateTaskTime(BaseDateTime, 10, False, 3));
end;

procedure TTaskCalendarTest.TestFormatLocalTime;
begin
  AssertEquals('20201205T000000Z', FormatLocalTime(EncodeDate(2020, 12, 05)));
  AssertEquals('20201205T010203Z', FormatLocalTime(EncodeDate(2020, 12, 05) + EncodeTime(1, 2, 3, 4)));
end;

initialization
  RegisterTest(TTaskCalendarTest);
end.

