msgid ""
msgstr "Content-Type: text/plain; charset=UTF-8"

#: mainwindow.saddtaskbuttonhint
#, object-pascal-format
msgid "Add task to %s"
msgstr ""

#: mainwindow.sdeletetaskcaption
msgid "Delete task"
msgstr ""

#: mainwindow.sdeletetaskmessage
#, object-pascal-format
msgid "Do you want to delete the task \"%s\"?"
msgstr ""

#: taboutform.caption
msgid "About"
msgstr ""

#: taboutform.closebutton.caption
msgid "Close"
msgstr ""

#: taboutform.copyrightlabel.caption
msgid "Copyright Herbert Reiter"
msgstr ""

#: taboutform.downloadlinklabel.caption
msgid "https://gitlab.com/moasda/task-organizer"
msgstr ""

#: taboutform.downloadtitlelabel.caption
msgid "Download + Source Code:"
msgstr ""

#: taboutform.licenselabel.caption
msgid "License: GPL-3.0-only"
msgstr ""

#: taboutform.titlelabel.caption
msgctxt "taboutform.titlelabel.caption"
msgid "Personal Kanban Task Organizer"
msgstr ""

#: taboutform.versionlabel.caption
msgid "Version: {}"
msgstr ""

#: taskeditdlg.sttaskstatuscancelled
msgid "Cancelled"
msgstr ""

#: taskeditdlg.sttaskstatuscompleted
msgid "Completed"
msgstr ""

#: taskeditdlg.sttaskstatusinprocess
msgid "In process"
msgstr ""

#: taskeditdlg.sttaskstatusneedsaction
msgid "Needs action"
msgstr ""

#: taskmodel.skanbangrouptitleblocked
msgid "Blocked"
msgstr ""

#: taskmodel.skanbangrouptitledone
msgid "Done"
msgstr ""

#: taskmodel.skanbangrouptitleidea
msgid "Ideas"
msgstr ""

#: taskmodel.skanbangrouptitlemonth
msgid "This month"
msgstr ""

#: taskmodel.skanbangrouptitlequarter
msgid "This quarter"
msgstr ""

#: taskmodel.skanbangrouptitletoday
msgid "Today"
msgstr ""

#: taskmodel.skanbangrouptitleweek
msgid "This week"
msgstr ""

#: taskmodel.skanbangrouptitleyear
msgid "This year"
msgstr ""

#: tkanbangroupform.cancelbutton.caption
msgctxt "tkanbangroupform.cancelbutton.caption"
msgid "Cancel"
msgstr ""

#: tkanbangroupform.caption
msgid "Edit Kanban Group"
msgstr ""

#: tkanbangroupform.closetaskscheckbox.caption
msgid "Close tasks when moved to this group"
msgstr ""

#: tkanbangroupform.icalendarexportcheckbox.caption
msgid "Export tasks in iCalendar"
msgstr ""

#: tkanbangroupform.okbutton.caption
msgctxt "tkanbangroupform.okbutton.caption"
msgid "OK"
msgstr ""

#: tkanbangroupform.titlelabel.caption
msgctxt "tkanbangroupform.titlelabel.caption"
msgid "Title:"
msgstr ""

#: tmainform.burgerbutton.hint
msgid "Show application menu"
msgstr ""

#: tmainform.burgermenuitemabout.caption
msgid "About..."
msgstr ""

#: tmainform.burgermenuitemsettings.caption
msgid "Settings..."
msgstr ""

#: tmainform.caption
msgctxt "tmainform.caption"
msgid "Personal Kanban Task Organizer"
msgstr ""

#: tmainform.kanbangroupmenuedit.caption
msgid "Edit Kanban Group..."
msgstr ""

#: tmainform.kanbangroupmenushowoldtasks.caption
msgid "Show old tasks"
msgstr ""

#: tmainform.searchedit.hint
msgid "Search for tasks"
msgstr ""

#: tmainform.taskmenudeleteitem.caption
msgid "Delete task..."
msgstr ""

#: tmainform.taskmenuedititem.caption
msgid "Edit task..."
msgstr ""

#: tsettingsform.alertdescriptionlabel.caption
msgid "minutes before the event"
msgstr ""

#: tsettingsform.alertlabel.caption
msgid "Show alert:"
msgstr ""

#: tsettingsform.cancelbutton.caption
msgctxt "tsettingsform.cancelbutton.caption"
msgid "Cancel"
msgstr ""

#: tsettingsform.caption
msgid "Settings"
msgstr ""

#: tsettingsform.explanationlabel.caption
msgid "The following texts are initially copied when you create a new task."
msgstr ""

#: tsettingsform.hideoldtaskscheckbox.caption
msgid "Hide old tasks after:"
msgstr ""

#: tsettingsform.hideoldtasksdayslabel.caption
msgid "days"
msgstr ""

#: tsettingsform.icalcheckbox.caption
msgid "iCalendar export enabled"
msgstr ""

#: tsettingsform.kanbangroupexportlabel.caption
msgid "Kanban Groups to export as iCalendar:"
msgstr ""

#: tsettingsform.languagelabel.caption
msgid "Language:"
msgstr ""

#: tsettingsform.minuteslabel.caption
msgid "minutes"
msgstr ""

#: tsettingsform.okbutton.caption
msgctxt "tsettingsform.okbutton.caption"
msgid "OK"
msgstr ""

#: tsettingsform.portlabel.caption
msgid "Server port:"
msgstr ""

#: tsettingsform.tabsheetgeneral.caption
msgid "General"
msgstr ""

#: tsettingsform.tabsheeticalendar.caption
msgid "iCalendar"
msgstr ""

#: tsettingsform.tabsheettemplate.caption
msgid "Task Template"
msgstr ""

#: tsettingsform.taskeditorfonttextlabel.caption
msgid "Font name"
msgstr ""

#: tsettingsform.taskeditorlabel.caption
msgid "Task Editor Font:"
msgstr ""

#: tsettingsform.taskeditorselectfontbutton.caption
msgid "Select Font..."
msgstr ""

#: tsettingsform.templatedetailslabel.caption
msgctxt "tsettingsform.templatedetailslabel.caption"
msgid "Details:"
msgstr ""

#: tsettingsform.templatetitlelabel.caption
msgctxt "tsettingsform.templatetitlelabel.caption"
msgid "Title:"
msgstr ""

#: tsettingsform.timeoffsetlabel.caption
msgid "Time offset to next task:"
msgstr ""

#: tsettingsform.transparencylabel.caption
msgid "Main window transparency:"
msgstr ""

#: tsettingsform.urlcopybutton.hint
msgid "Copy to clipboard"
msgstr ""

#: tsettingsform.urllabel.caption
msgid "Server URL for calendar clients:"
msgstr ""

#: tsettingsform.urllink.caption
msgid "http://localhost:12080/calendar.ics"
msgstr ""

#: tsettingsform.workfolderbutton.caption
msgid "Select Folder..."
msgstr ""

#: tsettingsform.workfolderlabel.caption
msgid "Work folder (contains tasks.json file):"
msgstr ""

#: tsettingsform.workfoldervaluelabel.caption
msgid "WorkFolderValueLabel"
msgstr ""

#: ttaskeditform.cancelbutton.caption
msgctxt "ttaskeditform.cancelbutton.caption"
msgid "Cancel"
msgstr ""

#: ttaskeditform.caption
msgid "Edit Task"
msgstr ""

#: ttaskeditform.completeddatelabel.caption
msgid "Completed Date:"
msgstr ""

#: ttaskeditform.createddatelabel.caption
msgid "Created Date:"
msgstr ""

#: ttaskeditform.detailslabel.caption
msgctxt "ttaskeditform.detailslabel.caption"
msgid "Details:"
msgstr ""

#: ttaskeditform.duedatelabel.caption
msgid "Due Date:"
msgstr ""

#: ttaskeditform.kanbangrouplabel.caption
msgid "Kanban Group:"
msgstr ""

#: ttaskeditform.lastmodifieddatelabel.caption
msgid "Last Modified Date:"
msgstr ""

#: ttaskeditform.okbutton.caption
msgctxt "ttaskeditform.okbutton.caption"
msgid "OK"
msgstr ""

#: ttaskeditform.startdatelabel.caption
msgid "Start Date:"
msgstr ""

#: ttaskeditform.tabsheet1.caption
msgid "Description"
msgstr ""

#: ttaskeditform.tabsheet2.caption
msgid "Status"
msgstr ""

#: ttaskeditform.taskidlabel.caption
msgid "Task ID:"
msgstr ""

#: ttaskeditform.taskstatuslabel.caption
msgid "Status:"
msgstr ""

#: ttaskeditform.titlelabel.caption
msgctxt "ttaskeditform.titlelabel.caption"
msgid "Title:"
msgstr ""
