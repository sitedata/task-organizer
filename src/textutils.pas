{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TextUtils;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, StdCtrls;

type
  CharSet = Set of Char;

function IsCodepointInRange(const Text: String; Codepoint: Integer; Range: CharSet): Boolean;
function WordPosBeforeCursor(const Text: String; Codepoint: Integer): Integer;
procedure DeleteWordBeforeCursor(Edit: TCustomEdit);

function DateTime2String(Date: TDateTime): String;
function String2DateTime(Str: String): TDateTime;

function EncodeLineBreaks(Str: String): String;
function DecodeLineBreaks(Str: String): String;

implementation

uses LazUTF8;

const
  WhiteSpace = [#0..' '] - [#10, #13];
  DateTimeFormatStrForJson = 'yyyy-mm-dd"T"hh:nn:ss"Z"';
  FormatSettingsForJson: TFormatSettings = (
    CurrencyFormat: 1;
    NegCurrFormat: 5;
    ThousandSeparator: ',';
    DecimalSeparator: '.';
    CurrencyDecimals: 2;
    DateSeparator: '-';
    TimeSeparator: ':';
    ListSeparator: ',';
    CurrencyString: '$';
    ShortDateFormat: 'yyyy-mm-dd';
    LongDateFormat: 'yyyy-mm-dd';
    TimeAMString: 'AM';
    TimePMString: 'PM';
    ShortTimeFormat: 'hh:nn:ss';
    LongTimeFormat: 'hh:nn:ss';
    ShortMonthNames: ('Jan','Feb','Mar','Apr','May','Jun',
                      'Jul','Aug','Sep','Oct','Nov','Dec');
    LongMonthNames: ('January','February','March','April','May','June',
                     'July','August','September','October','November','December');
    ShortDayNames: ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    LongDayNames:  ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    TwoDigitYearCenturyWindow: 50;
  );

{
  Check if the given codepoint character is in the given set.
}
function IsCodepointInRange(const Text: String; Codepoint: Integer; Range: CharSet): Boolean;
var CpChar: PChar;
begin
  CpChar := UTF8CodepointStart(PChar(Text), Text.Length, Codepoint);
  Result := (CpChar <> nil) and (CpChar^ in Range);
end;

{
  Compute the beginning of the word left from the cursor position.
  Ensures that a space between two words is always kept.

  CursorPos: Current cursor position codepoint to start with, starts with 0.
  Result: New cursor position codepoint, starts with 0.
}
function WordPosBeforeCursor(const Text: String; Codepoint: Integer): Integer;
begin
  // line break
  if (Codepoint > 0) and IsCodepointInRange(Text, Codepoint - 1, [#10]) then
  begin
    Dec(Codepoint);
    if (Codepoint > 0) and IsCodepointInRange(Text, Codepoint - 1, [#13]) then
    begin
      Dec(Codepoint);
    end;
    Exit(Codepoint);
  end;
  if (Codepoint > 0) and IsCodepointInRange(Text, Codepoint - 1, [#13]) then
  begin
    Dec(Codepoint);
    if (Codepoint > 0) and IsCodepointInRange(Text, Codepoint - 1, [#10]) then
    begin
      Dec(Codepoint);
    end;
    Exit(Codepoint);
  end;

  // delete white space to the left
  while (Codepoint > 0) and IsCodepointInRange(Text, Codepoint - 1, WhiteSpace) do
    Dec(Codepoint);
  // delete word to the left
  while (Codepoint > 0) and not IsCodepointInRange(Text, Codepoint - 1, WhiteSpace + [#10, #13]) do
    Dec(Codepoint);
  Result := Codepoint;
end;

{
  Deletes the word left before the cursor.
  Is called if user presses Ctrl+Backspace.
}
procedure DeleteWordBeforeCursor(Edit: TCustomEdit);
var LastStart: Integer;
begin
  LastStart := Edit.SelStart;
  Edit.SelStart := WordPosBeforeCursor(Edit.Text, LastStart);
  Edit.SelLength := LastStart - Edit.SelStart;
  Edit.SelText := '';
end;

{
  Date in ISO 8601 format: 2021-01-30T01:02:03Z
}
function DateTime2String(Date: TDateTime): String;
begin
  DateTimeToString(Result, DateTimeFormatStrForJson, Date, FormatSettingsForJson);
end;

{
  Date in ISO 8601 format: 2021-01-30T01:02:03Z
}
function String2DateTime(Str: String): TDateTime;
begin
  if Str.Length = 0 then
  begin
    Exit(0);
  end;

  // convert to format: 2021-01-30 01:02:03
  Str := Str.Replace('T', ' ');
  if Str.EndsWith('Z') then
  begin
    Str := Str.Substring(0, Str.Length - 1);
  end;
  Result := StrToDateTime(Str, FormatSettingsForJson);
end;

{
  Encodes all line breaks to convert a multi-line string
  to a single-line string.
}
function EncodeLineBreaks(Str: String): String;
begin
  Str := Str.Replace('\', '\\', [rfReplaceAll]);
  Result := Str.Replace(LineEnding, '\n', [rfReplaceAll]);
end;

{
  Reverses the transformation of EncodeLineBreaks().
}
function DecodeLineBreaks(Str: String): String;
var I: Integer;
begin
  I := 1;
  while I <= Str.Length do
  begin
    if (Str[I] = '\') and (I < Str.Length) then
    begin
      if Str[I + 1] = '\' then
      begin
        // we found '\\' -> replace by '\'
        System.Delete(Str, I, 1);
      end
      else if Str[I + 1] = 'n' then
      begin
        // we found '\n' -> replace by LineEnding
        System.Delete(Str, I, 2);
        System.Insert(LineEnding, Str, I);
        Inc(I, Length('' + LineEnding) - 1);
      end;
    end;
    Inc(I);
  end;
  Result := Str;
end;

end.

