{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TaskEditDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, IniPropStorage, ComCtrls, EditBtn, DateTimePicker, TaskModel;

type
  TTaskEditForm = class(TForm)
    CancelButton: TButton;
    CompletedDateEdit: TDateEdit;
    DueDateEdit: TDateEdit;
    StartDateEdit: TDateEdit;
    TaskStatusComboBox: TComboBox;
    KanbanGroupComboBox: TComboBox;
    CreatedDateTimePicker: TDateTimePicker;
    LastModifiedDateTimePicker: TDateTimePicker;
    DetailsEdit: TMemo;
    DetailsLabel: TLabel;
    IniPropStorage: TIniPropStorage;
    ButtonPanel: TPanel;
    TaskIdLabel: TLabel;
    TaskIdValue: TEdit;
    CreatedDateLabel: TLabel;
    LastModifiedDateLabel: TLabel;
    StartDateLabel: TLabel;
    DueDateLabel: TLabel;
    CompletedDateLabel: TLabel;
    TaskStatusLabel: TLabel;
    KanbanGroupLabel: TLabel;
    OkButton: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TitleEdit: TEdit;
    TitleLabel: TLabel;
    procedure DetailsEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DetailsEditKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure TaskStatusComboBoxChange(Sender: TObject);
    procedure TitleEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TitleEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TitleEditKeyPress(Sender: TObject; var Key: char);
  public
    function GetTask: TTask;
    procedure SetTask(Task: TTask; KanbanGroups: TKanbanGroupList);
    procedure CleanFields(KanbanGroups: TKanbanGroupList; KanbanGroupIndex: Integer);
  private
    CtrlPressed: Boolean;
    procedure EnsureDialogVisible;
    procedure LoadTranslations;
    procedure UpdateOkButton;
    function Zero2NullDate(Date: TDateTime): TDateTime;
    function NullDate2Zero(Date: TDateTime): TDateTime;
  end;

var
  TaskEditForm: TTaskEditForm;

implementation

uses
  Constants, MainWindow, TextUtils;

{$R *.lfm}

resourcestring
  STTaskStatusNeedsAction = 'Needs action';
  STTaskStatusInProcess = 'In process';
  STTaskStatusCompleted = 'Completed';
  STTaskStatusCancelled = 'Cancelled';

procedure TTaskEditForm.FormCreate(Sender: TObject);
begin
  IniPropStorage.IniFileName := MainForm.IniPropStorage.IniFileName;
end;

procedure TTaskEditForm.FormShow(Sender: TObject);
var FontName: String;
    FontSize: Integer;
begin
  EnsureDialogVisible;
  LoadTranslations;

  // Task edit font
  FontName := IniPropStorage.ReadString(SettingFontName, '');
  FontSize := IniPropStorage.ReadInteger(SettingFontSize, 0);
  WriteLog('TTaskEdit: Set FontName="' + FontName + '", FontSize=' + IntToStr(FontSize));
  if (FontName <> '') and (FontSize > 0) then
  begin
    DetailsEdit.Font.Name := FontName;
    DetailsEdit.Font.Size := FontSize;
  end;
  // Others
  UpdateOkButton;
  // Set focus
  PageControl1.ActivePageIndex := 0;
  if TitleEdit.Caption = '' then
  begin
    TitleEdit.SetFocus;
  end
  else
  begin
    DetailsEdit.SetFocus;
    DetailsEdit.SelStart := Length(DetailsEdit.Text);
  end;
end;

procedure TTaskEditForm.TitleEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  CtrlPressed := ssCtrl in Shift;
end;

procedure TTaskEditForm.TitleEditKeyPress(Sender: TObject; var Key: char);
begin
  {$IFDEF MSWINDOWS}
  // Ctrl+Backspace pressed, delete word before cursor
  // Windows sends #127, Linux sends #8 = VK_BACK
  if Key in [#127] then
  begin
    DeleteWordBeforeCursor(TitleEdit);
    Key := #0;
  end;
  {$ENDIF}
end;

procedure TTaskEditForm.TitleEditChange(Sender: TObject);
begin
  UpdateOkButton;
end;

procedure TTaskEditForm.DetailsEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  CtrlPressed := ssCtrl in Shift;
end;

procedure TTaskEditForm.DetailsEditKeyPress(Sender: TObject; var Key: char);
begin
  {$IFDEF MSWINDOWS}
  // Ctrl+Backspace pressed, delete word before cursor
  // Windows sends #127, Linux sends #8 = VK_BACK
  if Key in [#127] then
  begin
    DeleteWordBeforeCursor(DetailsEdit);
    Key := #0;
  end;
  {$ENDIF}
end;

procedure TTaskEditForm.TaskStatusComboBoxChange(Sender: TObject);
var TaskStatus: TTaskStatus;
begin
  TaskStatus := TTaskStatus(TaskStatusComboBox.ItemIndex);
  if TaskStatus in [Completed, Cancelled] then
  begin
    // User wants to close the task
    CompletedDateEdit.Date := Now;
  end;
end;

{
  Returns a new task object, has to be freed afterwards.
}
function TTaskEditForm.GetTask: TTask;
begin
  Result := TTask.Create;
  Result.Id := 0; // will be set later
  Result.Title := TitleEdit.Caption;
  Result.Details := DetailsEdit.Caption;
  Result.StartDate := StartDateEdit.Date;
  Result.DueDate := DueDateEdit.Date;
  Result.CompletedDate := CompletedDateEdit.Date;
  Result.Status := TTaskStatus(TaskStatusComboBox.ItemIndex);
  Result.KanbanGroupIndex := KanbanGroupComboBox.ItemIndex;
end;

procedure TTaskEditForm.SetTask(Task: TTask; KanbanGroups: TKanbanGroupList);
begin
  CleanFields(KanbanGroups, Task.KanbanGroupIndex);
  TitleEdit.Caption := Task.Title;
  DetailsEdit.Caption := Task.Details;
  TaskIdValue.Text := IntToStr(Task.Id);
  CreatedDateTimePicker.DateTime := Zero2NullDate(Task.CreatedDate);
  LastModifiedDateTimePicker.DateTime := Zero2NullDate(Task.LastModifiedDate);
  StartDateEdit.Date := Task.StartDate;
  DueDateEdit.Date := Task.DueDate;
  CompletedDateEdit.Date := Task.CompletedDate;
  TaskStatusComboBox.ItemIndex := Ord(Task.Status);
end;

{
  Is called for new tasks.
}
procedure TTaskEditForm.CleanFields(KanbanGroups: TKanbanGroupList; KanbanGroupIndex: Integer);
var KanbanGroup: TKanbanGroup;
    KanbanGroupTitle: String;
begin
  TitleEdit.Caption := IniPropStorage.ReadString(SettingTemplateTitle, '');
  DetailsEdit.Caption := DecodeLineBreaks(IniPropStorage.ReadString(SettingTemplateDetails, ''));
  TaskIdValue.Text := '';
  CreatedDateTimePicker.DateTime := NullDate;
  LastModifiedDateTimePicker.DateTime := NullDate;
  StartDateEdit.Date := EditBtn.NullDate;
  DueDateEdit.Date := EditBtn.NullDate;
  CompletedDateEdit.Date := EditBtn.NullDate;
  TaskStatusComboBox.ItemIndex := 0;
  KanbanGroupComboBox.Clear;
  for KanbanGroup in KanbanGroups do
  begin
    if (KanbanGroup.Title <> '') then
      KanbanGroupTitle := KanbanGroup.Title
    else
      KanbanGroupTitle := GetDefaultGroupTitle(KanbanGroup.Index);
    KanbanGroupComboBox.AddItem(KanbanGroupTitle, nil);
  end;
  KanbanGroupComboBox.ItemIndex := KanbanGroupIndex;
end;

{
  Initially the dialog is centered over the main window.
  This can lead to some parts of the dialog to be outside
  of the screen.
  This method ensures that the dialog is fully visible.
}
procedure TTaskEditForm.EnsureDialogVisible;
var WorkareaRect: TRect;
begin
  WorkareaRect := Monitor.WorkareaRect;
  if Left < WorkareaRect.Left then
  begin
    Left := WorkareaRect.Left;
  end;
  if Top < WorkareaRect.Top then
  begin
    Top := WorkareaRect.Top;
  end;
  if Left + Width > WorkareaRect.Right then
  begin
    Left := WorkareaRect.Right - Width;
  end;
  if Top + Height > WorkareaRect.Bottom then
  begin
    Top := WorkareaRect.Bottom - Height;
  end;
end;

procedure TTaskEditForm.LoadTranslations;
begin
  TaskStatusComboBox.Items[0] := STTaskStatusNeedsAction;
  TaskStatusComboBox.Items[1] := STTaskStatusInProcess;
  TaskStatusComboBox.Items[2] := STTaskStatusCompleted;
  TaskStatusComboBox.Items[3] := STTaskStatusCancelled;
end;

procedure TTaskEditForm.UpdateOkButton;
begin
  OkButton.Enabled := Length(TitleEdit.Caption) > 0;
end;

{
  Task.DateTime = 0 means "no date",
  while DateTimePicker uses the value DateTimePicker.NullDate
}
function TTaskEditForm.Zero2NullDate(Date: TDateTime): TDateTime;
begin
  if Date = 0 then
  begin
    // Required for DateTimePicker
    Result := DateTimePicker.NullDate;
  end
  else
  begin
    Result := Date;
  end;
end;

{
  Task.DateTime = 0 means "no date",
  while DateTimePicker uses the value DateTimePicker.NullDate
}
function TTaskEditForm.NullDate2Zero(Date: TDateTime): TDateTime;
begin
  if Date = DateTimePicker.NullDate then
  begin
    // Required for DateTimePicker
    Result := 0;
  end
  else
  begin
    Result := Date;
  end;
end;

end.

