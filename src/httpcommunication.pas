{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit HttpCommunication;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, blcksock;

const
  CalendarUrlPath = '/calendar.ics';

type
  THttpRequest = record
    HttpMethod: String;
    Path: String;
    ClientName: String;
  end;

  THttpResponse = record
    StatusCode: Integer;
    ContentType: String;
    Data: String;
  end;

  TOnHttpRequest = procedure(Sender: TObject; HttpRequest: THttpRequest;
                 var HttpResponse: THttpResponse) of object;
  TRequestHandlerThread = class;

  THttpServer = class
  private
    FPort: Integer;
    FOnHttpRequest: TOnHttpRequest;
    RequestHandlerThread: TRequestHandlerThread;

  public
    constructor Create;
    destructor Destroy; override;
    procedure Start;
    procedure Stop;
    function IsRunning: Boolean;
    property Port: Integer read FPort write FPort;
    property OnHttpRequest: TOnHttpRequest write FOnHttpRequest;
  end;

  TRequestHandlerThread = class(TThread)
  private
    FPort: Integer;
    FOnHttpRequest: TOnHttpRequest;
    FProgramVersion: String;

    procedure HandleRequest(ConnectionSocket: TTCPBlockSocket);
  protected
    procedure Execute; override;
    procedure WriteHttpResponse(ConnectionSocket: TTCPBlockSocket; HttpResponse: THttpResponse);
  public
    constructor Create(Port: Integer; OnHttpRequest: TOnHttpRequest);
  end;

implementation

uses Constants, TextUtils;

function ParseHttpRequest(DataStr: String): THttpRequest;
var Pos1, Pos2: Integer;
begin
  Pos1 := DataStr.IndexOf(' ');
  Pos2 := DataStr.IndexOf(' ', Pos1 + 1);
  Result.HttpMethod := '';
  if Pos1 > 0 then
     Result.HttpMethod := DataStr.Substring(0, Pos1);
  Result.Path := '';
  if (Pos1 > 0) and (Pos2 > 0) then
     Result.Path := DataStr.Substring(Pos1 + 1, Pos2 - Pos1 - 1);
  Result.ClientName := '';
end;

function HttpStatusCodeText(StatusCode: Integer): String;
begin
  case StatusCode of
    200: Result := 'OK';
    404: Result := 'Not Found';
    else Result := '';
  end;
end;

// ************************ TRequestHandlerThread ************************

constructor TRequestHandlerThread.Create(Port: Integer; OnHttpRequest: TOnHttpRequest);
begin
  inherited Create(True); // create suspended
  FPort := Port;
  FOnHttpRequest := OnHttpRequest;
  FProgramVersion := GetProgramVersion;
  Start;
end;

procedure TRequestHandlerThread.Execute;
var ListenerSocket, ConnectionSocket: TTCPBlockSocket;
begin
  try
    try
      ListenerSocket := TTCPBlockSocket.Create;
      ListenerSocket.CreateSocket;
      ListenerSocket.Bind('0.0.0.0', IntToStr(FPort));
      ListenerSocket.Listen;
      WriteLog('HttpServer listening on port ' + IntToStr(FPort));

      ConnectionSocket := TTCPBlockSocket.Create;
      while not Terminated do
      begin
        if not ListenerSocket.CanRead(1000) then
          Continue;
        ConnectionSocket.Socket := ListenerSocket.Accept;
        HandleRequest(ConnectionSocket);
        ConnectionSocket.CloseSocket;
      end;

    except
      on E: Exception do
      begin
        WriteLog('Error starting HttpServer: ' + E.ClassName + ': ' + E.Message);
      end;
    end;

  finally
    if ConnectionSocket <> nil then
      ConnectionSocket.Free;
    if ListenerSocket <> nil then
      ListenerSocket.Free;
  end;
  WriteLog('HttpServer execution thread exits');
end;

procedure TRequestHandlerThread.HandleRequest(ConnectionSocket: TTCPBlockSocket);
var DataStr: String;
    HttpRequest: THttpRequest;
    HttpResponse: THttpResponse;
begin
  if ConnectionSocket.LastError <> 0 then
  begin
    WriteLog('LastError = ' + IntToStr(ConnectionSocket.LastError) + ', aborting');
    Exit;
  end;
  // Parse request data
  DataStr := ConnectionSocket.RecvString(1000);
  HttpRequest := ParseHttpRequest(DataStr);
  WriteLog('Incoming HTTP request: ' + HttpRequest.HttpMethod + ' ' + HttpRequest.Path);
  // Process request
  HttpResponse.StatusCode := 200;
  HttpResponse.ContentType := '';
  HttpResponse.Data := '';
  if FOnHttpRequest <> nil then
    FOnHttpRequest(self, HttpRequest, HttpResponse);
  // Send response date
  WriteLog('Sending response: status code = ' + IntToStr(HttpResponse.StatusCode)
                   + ', ' + IntToStr(HttpResponse.Data.Length) + ' bytes');
  WriteHttpResponse(ConnectionSocket, HttpResponse);
end;

procedure TRequestHandlerThread.WriteHttpResponse(ConnectionSocket: TTCPBlockSocket; HttpResponse: THttpResponse);
begin
  ConnectionSocket.SendString('HTTP/1.0 '
    + IntToStr(HttpResponse.StatusCode) + ' '
    + HttpStatusCodeText(HttpResponse.StatusCode) + CRLF);
  ConnectionSocket.SendString('Server: ' + ProgramName + '/' + FProgramVersion + CRLF);
  if HttpResponse.ContentType.IsEmpty then
    ConnectionSocket.SendString('Content-Type: text/plain' + CRLF)
  else
    ConnectionSocket.SendString('Content-Type: ' + HttpResponse.ContentType + CRLF);
  ConnectionSocket.SendString('Content-Length: ' + IntToStr(HttpResponse.Data.Length) + CRLF);
  ConnectionSocket.SendString('Connection: Close' + CRLF);
  ConnectionSocket.SendString(CRLF);
  ConnectionSocket.SendString(HttpResponse.Data);
end;

// ************************ THttpServer ************************

constructor THttpServer.Create;
begin
  inherited;
end;

destructor THttpServer.Destroy;
begin
  inherited;
end;

procedure THttpServer.Start;
begin
  if RequestHandlerThread = nil then
  begin
    WriteLog('HttpServer starting');
    RequestHandlerThread := TRequestHandlerThread.Create(FPort, FOnHttpRequest);
    RequestHandlerThread.Start;
  end;
end;

procedure THttpServer.Stop;
begin
  if RequestHandlerThread <> nil then
  begin
    RequestHandlerThread.Terminate;
    RequestHandlerThread.WaitFor;
    FreeAndNil(RequestHandlerThread);
    WriteLog('HttpServer stopped');
  end;
end;

function THttpServer.IsRunning: Boolean;
begin
  Result := (RequestHandlerThread <> nil);
end;

end.

